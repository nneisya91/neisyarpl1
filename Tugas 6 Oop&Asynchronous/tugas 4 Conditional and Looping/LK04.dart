import 'dart:io';

void main(List<String> args) {
    print("Apakah Ingin Menginstall Aplikasi ?");

    String? pilihan = stdin.readLineSync()!;
    (pilihan == 'y')
    ? print("Instalasi Berjalan")
    : print("Instalasi Dibatalkan");
}