void main(List<String> args) {
  dataHandling(input);
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/05/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Sultan Badra", "Martapura", "06/04/1970", "Berkebun"],
];

dataHandling(data) {
  var datalength = data.length;
  for (var i = 0; i < datalength; i++) {
    var id = "Nomor ID : " + data[i][0];
    var nama = "Nama Lengkap : " + data[i][1];
    var ttl = "TTL : " + data[i][2] + ", " + data [i][3];
    var hobi = "Hobi : " + data[i][4];

    print(id);
    print(nama);
    print(ttl);
    print(hobi);
    print("-------------------------------------------------------------");
  }
}
